#ifndef BACKTRACK_H
#define BACKTRACK_H

#include "csp.h"

/**
 * Backtrack de base
 * Parametre: un csp sur lequel nous allons faire des affectations successives tant qu'eslles
 *  sont consistantes.
 *
 *  return 1 si backtrack a trouvé la solution, -1 sinon.
 *  TODO: par la suite il faudrait retourner la grille avec les valeurs dans les cases
 */
int backtrack_base(CSP csp);

/**
 * Affectation d'une valeur a une case, regarder si cette affectation est consistante
 *
 * Renvoie -1 en cas de'affectation non consistante (qui viole une ou plusieurs contraintes)
 */
int affectation(CSP csp, int valeur);

/**
 * Solution
 *
 * Renvoie -1 si ce n'est pas la solution, 1 sinon
 */
int solution(CSP csp);

/**
 * Backtrack avec heuristique
 */

/**
 * Analyser les echecs
 * identifier les causes des echecs, modifier l'affectation d'une variable
 *  en cause dans l'echec
 * "retour en arriere intelligent
 */

/**
 * Memoriser les echecs
 */

#endif //PROJET_SAKURO_BACKTRACK_H
