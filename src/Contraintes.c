#include "Contraintes.h"

Contraintes *new_contraintes(int val, Sens sens){
    Contraintes *new_contraintes;
    new_contraintes=(Contraintes*) malloc(sizeof(Contraintes));
    new_contraintes->val_contrainte=val;
    new_contraintes->id_cases=(int*) malloc(10*sizeof(int));
    int i;
    for(i=0;i<10;++i){
        new_contraintes->id_cases[i]=-1;
    }
    new_contraintes->sens=sens;
    new_contraintes->next=NULL;
    new_contraintes->nb_cases=0;

    return new_contraintes;
}

Contraintes* new_empty_contraintes(){
    Contraintes* new_c;
    new_c=(Contraintes*) malloc(sizeof(Contraintes));
    new_c->val_contrainte=-1;
    new_c->next=NULL;

    return new_c;
}


int get_next_id_cases(Contraintes *c){
    int i;
    for(i=0;i<10;++i){
        if(c->id_cases[i]==-1) 
            return i;
    }
    return -1;
}

void free_contraintes(Contraintes *cnt){
    Contraintes *tmp=cnt;
    while((tmp=cnt) != NULL){
        cnt=cnt->next;
        free_contraintes(tmp);
    }
    free(cnt->id_cases);
    cnt->id_cases=NULL;    
    free(cnt->next);
    cnt->next=NULL;
    free(cnt);
}
