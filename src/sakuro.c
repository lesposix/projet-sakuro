#include <stdlib.h>
#include "csp.h"
#include "lecteur_grille.h"

void afficher_grille(Grille *grille){
    printf("Colonnes:%d\n",grille->nbCol);
    printf("Lignes:%d\n",grille->nbLigne);
    int i,j;
    for(i=0;i<grille->nbLigne;++i){
        for(j=0;j<grille->nbCol;++j){
            Case *c=getGrilleValue(grille,i,j);
            if(c->couleur == NOIR){
                if(c->contraintes!= NULL){
                    if(c->contraintes->sens== VERTICAL){
                        if(c->contraintes->next != NULL)
                            printf("%d\\%d",c->contraintes->val_contrainte,c->contraintes->next->val_contrainte);
                        else
                            printf("%d\\",c->contraintes->val_contrainte);
                    }else
                        printf("\\%d",c->contraintes->val_contrainte);
                } else
                    printf("\\");
            }
            else if (c->couleur == BLANC){
                if(c->val != 0)
                    printf("%d\n",c->val);
                else
                    printf(".");
            }
            printf("\t");
        }
        printf("\n");
    }
}

void afficher_contraintes_blanches(Grille *g){
   Case* c_tmp;
   Contraintes *cnt_tmp;
   int i,j;
   for(i=0;i<g->nbLigne;++i){
       for(j=0;j<g->nbCol;++j){
           c_tmp=getGrilleValue(g,i,j);
           if(c_tmp->couleur == BLANC){
               cnt_tmp=c_tmp->contraintes;
               printf("Case blanche id:%d\n",c_tmp->id_case);
               while(cnt_tmp->next != NULL){
                   printf("%d, ",cnt_tmp->val_contrainte);
                   cnt_tmp=cnt_tmp->next;
               }
               printf("%d\n",cnt_tmp->val_contrainte);
           }
       }
   }

}

void afficher_relations(Grille *g){
   Case* c_tmp;
   Contraintes *cnt_tmp;
   int i,j;
   for(i=0;i<g->nbLigne;++i){
       for(j=0;j<g->nbCol;++j){
           c_tmp=getGrilleValue(g,i,j);
           if(c_tmp->couleur == BLANC){
               cnt_tmp=c_tmp->contraintes;
               while(cnt_tmp->next != NULL){
		   int k;
		   printf("nb_cases:%d for id_case:%d\n",cnt_tmp->nb_cases,c_tmp->id_case);
		   for(k=0;k<cnt_tmp->nb_cases;++k)
			   printf("%d, ",cnt_tmp->id_cases[k]);
                   cnt_tmp=cnt_tmp->next;
               }
		printf("\n");
           }
       }
   }
}

/*
void afficher_relations(CSP *csp){
   Case* c_tmp;
   Contraintes *cnt_tmp;
   int i;
   for(i=0;i<csp->nbCaseBlanche;++i){
           c_tmp=csp->variable[i];
               cnt_tmp=c_tmp->contraintes;
               while(cnt_tmp->next != NULL){
		   int k;
		   for(k=0;k<get_next_id_cases(cnt_tmp);++k)
			   printf("%d, ",cnt_tmp->id_cases[k]);
                   cnt_tmp=cnt_tmp->next;
               }
		printf("\n");
           }
   }
*/

int main(int argc, char *argv[]){
    if(argc != 2){
        printf("Erreur d'arguments \n");
        exit(EXIT_FAILURE);
    }
    printf("Ouverture du fichier: %s \n",argv[1]);
    ouvrir_fichier_grille(argv[1]);
    printf("Fichier ouvert \n");

    Grille *grille=NULL;
    CSP *csp=NULL;

    printf("Initialisation de la grille \n");
    grille=init_grille(argv[1]);
    afficher_grille(grille);

    printf("Fermeture du fichier de grille\n");
    fermer_fichier_grille();

    csp=init_csp(&grille);
    printf("nb case blanches:%d\n",csp->nbCaseBlanche);
    printf("Grille initialisée\n");
    afficher_contraintes_blanches(grille);
    afficher_relations(grille);
 //   free_grille(grille);
    //free_csp(csp);

    return 0;
}

