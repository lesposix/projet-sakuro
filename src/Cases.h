#ifndef CASES_H
#define CASES_H

#include <stdlib.h>
#include "Contraintes.h"

typedef enum Couleur{NOIR, BLANC} Couleur;

typedef struct Case{
    Couleur couleur; 
    int id_case;
    int val;
    Contraintes *contraintes;
} Case;

Case *new_case(Couleur couleur, Contraintes *contraintes);

void add_contraintes(Case **c, Contraintes **new_contraintes);

void free_case(Case *c);


#endif
