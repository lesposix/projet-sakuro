#include "Cases.h"
#include "Contraintes.h"

int count=0;
Case *new_case(Couleur couleur, Contraintes *contraintes){
    Case *new_case;

    new_case=(Case*) malloc(sizeof(Case));
    new_case->couleur=couleur;
    new_case->contraintes=contraintes;
    new_case->val=0;
    if(new_case->couleur == BLANC){
        new_case->id_case=count;
        count+=1;
    }
    else 
        new_case->id_case=-1;

    return new_case;
}

void add_contraintes(Case **c, Contraintes **new_contrainte){
    Contraintes *tmp;
    tmp=(*c)->contraintes;
    if((*c)->contraintes == NULL){
        (*c)->contraintes=new_contraintes((*new_contrainte)->val_contrainte,(*new_contrainte)->sens);
	(*c)->contraintes->id_cases=(*new_contrainte)->id_cases;
	(*c)->contraintes->nb_cases=(*new_contrainte)->nb_cases;
    }
    else{
        while(tmp->next!=NULL){
            tmp=tmp->next; 
        }
        Contraintes *n_c;/*
        n_c=new_contraintes((*new_contrainte)->val_contrainte,(*new_contrainte)->sens);
	n_c->id_cases=(*new_contrainte)->id_cases;
	n_c->nb_cases=(*new_contrainte)->nb_cases;
*/
n_c=(*new_contrainte);
        tmp->next=n_c;
    }
}

void free_case(Case *c){
    c->contraintes=NULL;
    free(c);
}
