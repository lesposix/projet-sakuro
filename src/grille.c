#include "grille.h"

Grille* creer_n_grille(int nbLigne, int nbCol){
	Grille* grille;
    grille=(Grille*) malloc(sizeof(Grille));
	grille->nbCol=nbCol;
	grille->nbLigne=nbLigne;
	grille->matrix=(Case**)malloc(nbLigne * sizeof(Case*));
	
	int i;
	for(i=0;i<nbCol;++i){
		grille->matrix[i] = (Case*) calloc(nbLigne, sizeof(Case));
	}
	
	return grille;
}

Case *getGrilleValue(Grille *grille, int nbLigne, int nbCol){
    Case *c=&(grille->matrix[nbLigne][nbCol]);
    return c;
}

void setGrilleValue(Grille *grille, int nbLigne, int nbCol, Case *val){
	grille->matrix[nbLigne][nbCol]=*val;
}

void free_grille(Grille *g){
   int i; 

   for(i=0;i<g->nbLigne;++i){
        free_case(g->matrix[i]);
        g->matrix[i]=NULL;
   }
   free(g->matrix);
   g->matrix=NULL;
   free(g);
}

