#ifndef LECTEUR_GRILLE_H
#define LECTEUR_GRILLE_H

#include <stdlib.h>
#include <stdio.h>
#include "grille.h"

#define FIN 10
#define ESPACE 0
#define LIGNE 1
#define POINT 2
#define AS 3
#define NOMBRE 4

void ouvrir_fichier_grille(const char *fileName);
void fermer_fichier_grille();
char lireCar(void);
void delireCar();
Grille *init_grille(char *path);
int yylex();


#endif
