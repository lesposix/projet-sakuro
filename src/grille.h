#ifndef GRILLE_H
#define GRILLE_H

#include <stdlib.h>
#include "Cases.h"

typedef struct Grille{
		int nbCol;
		int nbLigne;
		Case** matrix;
} Grille;

Grille* creer_n_grille(int nbLigne,int nbCol);
Case *getGrilleValue(Grille *grille, int nbLigne,int nbCol);
void setGrilleValue(Grille *grille, int nbLigne, int nbCol, Case *val);
void free_grille(Grille *g);

#endif
