#include "lecteur_grille.h"
#include "grille.h"
#include "Cases.h"
#include "Contraintes.h"

#define is_num(c) (('0' <= (c)) && ((c) <= '9'))
#define max(X, Y) (((X) > (Y)) ? (X) : (Y))

FILE *grille_entree;
int yyleng;
char yytext[100];

void ouvrir_fichier_grille(const char *fileName) {
    grille_entree = fopen(fileName, "r");
    if (grille_entree == NULL) {
        printf("Erreur pour ouvrir le fichier: %s \n", fileName);
        exit(EXIT_FAILURE);
    }
}

void fermer_fichier_grille() { fclose(grille_entree); }

char lireCar(void) {
    yytext[yyleng++] = fgetc(grille_entree);
    yytext[yyleng] = '\0';
    return yytext[yyleng - 1];
}

void delireCar() {
    char c;
    c = yytext[yyleng - 1];
    yytext[--yyleng] = '\0';
    ungetc(c, grille_entree);
}

Grille *init_grille(char *path) {
    Grille *grille = NULL;
    int ligne = 0, colonne = 0;
    int colonneTemp = 0;

    int r;
    while ((r = yylex()) != FIN) {
        if (r == ESPACE) {
            ++colonneTemp;
        } else if (r == LIGNE) {
            ++ligne;
            colonne = max(colonne, colonneTemp);
            colonneTemp = 0;
        }
    }
    grille=creer_n_grille(ligne,colonne+1);
    fermer_fichier_grille();
    ouvrir_fichier_grille(path);

    ligne = 0, colonne = 0;
    r = yylex();
    Case *n_case;
    while (r != FIN) {
        if (r == NOMBRE) {
            int nb1 = atoi(yytext);
            r = yylex();
            if (r == AS) {
                r = yylex();
                if (r == NOMBRE) {
                    int nb2 = atoi(yytext);
                    r = yylex();
                    Contraintes *c1 = new_contraintes(nb1, VERTICAL);
                    Contraintes *c2 = new_contraintes(nb2, HORIZONTAL);
                    c1->next = c2;
                    n_case = new_case(NOIR, c1);
                    setGrilleValue(grille, ligne, colonne, n_case);
                    free(n_case);
                } else {
                    Contraintes *c1 = new_contraintes(nb1, VERTICAL);
                    n_case = new_case(NOIR, c1);
                    setGrilleValue(grille, ligne, colonne, n_case);
                    free(n_case);
                }
            }
            ++colonne;
        } else if (r == AS) {
            r = yylex();
            if (r == NOMBRE) {
                int nb = atoi(yytext);
                r = yylex();
                Contraintes *c = new_contraintes(nb, HORIZONTAL);
                n_case = new_case(NOIR, c);
                setGrilleValue(grille, ligne, colonne, n_case);
                free(n_case);
            } else {
                n_case = new_case(NOIR, NULL);
                setGrilleValue(grille, ligne, colonne, n_case);
                free(n_case);
            }
            ++colonne;
        } else if (r == POINT) {
            r = yylex();
            n_case = new_case(BLANC, NULL);
            setGrilleValue(grille, ligne, colonne, n_case);
            ++colonne;
            free(n_case);
        } else if (r == LIGNE) {
            r = yylex();
            colonne = 0;
            ++ligne;
        } else if (r == ESPACE) {
            r = yylex();
        }
        n_case = NULL;
    }

    return grille;
}

int yylex() {
    char c;
    yytext[yyleng = 0] = '\0';

    c = lireCar();

    if (c == EOF) {
        delireCar();
        return FIN;
    }

    if (c == ' ') {
        return ESPACE;
    } else if (c == '\n') {
        return LIGNE;
    } else if (c == '.') {
        return POINT;
    } else if (c == '\\') {
        return AS;
    } else if (is_num(c)) {
        while (is_num(c)) {
            c = lireCar();
        }
        delireCar();
        return NOMBRE;
    }
    return -1;
}
