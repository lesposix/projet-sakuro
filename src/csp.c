#include <string.h>
#include "csp.h"
#include <stdio.h>

CSP *init_csp(Grille **g) {
    CSP *csp = NULL;
    csp = malloc(sizeof(CSP));
    csp->nbCaseBlanche = 0;
    int x, y, init = 0;
    for (x = 0; x < (*g)->nbLigne; ++x) {
        for (y = 0; y < (*g)->nbCol; ++y) {
            Case *c = getGrilleValue(*g, x, y);
            if (c->couleur == BLANC) ++init;
        }
    }

    csp->variable = malloc((init) * sizeof(Case *));
    int i, j;
    for (i = 0; i < (*g)->nbLigne; ++i) {
        for (j = 0; j < (*g)->nbCol; ++j) {
            Case *c = getGrilleValue(*g, i, j);
            if (c->couleur == NOIR) {
                cnt_sur_blanc(g, i, j);
            } else if (c->couleur == BLANC) {
                csp->variable[csp->nbCaseBlanche] = c;
                ++csp->nbCaseBlanche;
            }
        }
    }
    return csp;
}

void cnt_sur_blanc(Grille **g, int i, int j) {
    Case *c = getGrilleValue((*g), i, j);
    Contraintes *cont = c->contraintes;

    while (cont != NULL) {
        int nbLigne = i, nbCol = j;
        Case *c_tmp;
        if (cont->sens == HORIZONTAL) {
            ++nbCol;
            c_tmp = getGrilleValue((*g), nbLigne, nbCol);
            while (nbCol < (*g)->nbCol && c_tmp->couleur != NOIR) {
                link_case_somme(&c_tmp, &cont);
                ++nbCol;
		if(nbCol < (*g)->nbCol)
			c_tmp = getGrilleValue((*g), nbLigne, nbCol);
            }
        } else {
            ++nbLigne;
            c_tmp = getGrilleValue((*g), nbLigne, nbCol);
            while (nbLigne < (*g)->nbLigne && c_tmp->couleur != NOIR) {
                link_case_somme(&c_tmp, &cont);
                ++nbLigne;
		if(nbLigne < (*g)->nbLigne)
			c_tmp = getGrilleValue((*g), nbLigne, nbCol);
            }
        }
        cont = cont->next;
    }
}

void link_case_somme(Case **c, Contraintes **cnt) {
    int i = get_next_id_cases(*cnt);
    (*cnt)->id_cases[i] = (*c)->id_case;
    ++(*cnt)->nb_cases;
    add_contraintes(c, cnt);
}

void free_csp(CSP *csp) {
    int i;

    for (i = 0; i < csp->nbCaseBlanche; ++i) {
        free(csp->variable[i]);
    }
    free(csp->variable);
    free(csp);
}

