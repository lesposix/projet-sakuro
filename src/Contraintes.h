#ifndef CONTRAINTES_H
#define CONTRAINTES_H

#include <stdlib.h>

typedef enum Sens{HORIZONTAL, VERTICAL} Sens;

typedef struct Contraintes{
    struct Contraintes *next;
    int val_contrainte;
    int *id_cases;
    int nb_cases;
    Sens sens;
} Contraintes;

Contraintes *new_contraintes(int val, Sens sens );
Contraintes *new_empty_contraintes();
int get_next_id_cases(Contraintes *c);
void free_contraintes(Contraintes *cnt);


#endif
