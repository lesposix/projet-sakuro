#ifndef CSP_H
#define CSP_H

#include "Cases.h" 
#include "grille.h"

typedef struct CSP{
    int nbCaseBlanche;
    Case** variable;
    //Contrainte
}CSP;

CSP* init_csp(Grille **g);
void link_case_somme(Case **c, Contraintes **cnt);
void link_case_diff();
void cnt_sur_blanc(Grille **g, int i, int j);
void free_csp(CSP *csp);


#endif
